# CATALYST Market Clearing Manager - Flexibility Market

## Installation instructions 

To install, Docker and Docker-compose support is assumed. 
Before building the container, you should configure the parameter AllSessionsCompleteTime, for the time of Completion Phase of Flexibility Market Sessions, the parameter FlexSessionsBillingTime for the time of Billing Phase, the parameters fixedFee e penalty for the Fee and Penalty related to Flexibility Market Sessions.

After successful docker installation and MEMO configuration, just run the commands:

```bash
$ cd Catalyst_MCM_Ancillary_Services/docker
$ bash launch.sh
```

If everything went well, the output should be similar to:

```bash
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS                      PORTS                    NAMES
7844d2473600        tomcat:8                              "catalina.sh run"        10 minutes ago      Up 10 minutes               0.0.0.0:8001->8080/tcp   tomcat
7a9b3960ac16        catalyst_marketplace_msm              "/usr/local/bin/mvn-…"   10 minutes ago      Exited (0) 10 minutes ago                            mcm_as
```

### How to install Docker under Ubuntu

To install docker / docker-compose in an ubuntu 16.04 system, run the following as root:

```bash
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
sudo chmod +x /usr/local/bin/docker-compose
```

### Docker-compose Installation

```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

