package eu.catalyst.mcm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mcm.payload.MarketAction;
import eu.catalyst.mcm.payload.MarketActionCounterOffer;
import eu.catalyst.mcm.payload.MarketSession;
import eu.catalyst.mcm.payload.Marketplace;
import eu.catalyst.mcm.payload.PayloadMEMO2MCM;
import eu.catalyst.mcm.utilities.Utilities;

@Stateless
@Path("/postClearingProcess/")
public class PostClearingProcess {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PostClearingProcess.class);

	public PostClearingProcess() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public PayloadMEMO2MCM postClearingProcess(PayloadMEMO2MCM myPayloadMEMO2MCM) {
		PayloadMEMO2MCM returnPayloadMEMO2MCM = new PayloadMEMO2MCM();
		returnPayloadMEMO2MCM.setInformationBrokerServerUrl(myPayloadMEMO2MCM.getInformationBrokerServerUrl());
		returnPayloadMEMO2MCM.setTokenMEMO(myPayloadMEMO2MCM.getTokenMEMO());
		returnPayloadMEMO2MCM.setTokenMCM(myPayloadMEMO2MCM.getTokenMCM());
		returnPayloadMEMO2MCM.setMarketClearingManagerUrl(myPayloadMEMO2MCM.getMarketClearingManagerUrl());
		returnPayloadMEMO2MCM.setMarketActionList(myPayloadMEMO2MCM.getMarketActionList());

		List<MarketAction> marketActionNotBilledList = myPayloadMEMO2MCM.getMarketActionList();

		ArrayList<MarketActionCounterOffer> returnMarketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();

		try {
			if (!marketActionNotBilledList.isEmpty()) {

				for (Iterator<MarketAction> iterator = marketActionNotBilledList.iterator(); iterator.hasNext();) {

					List<MarketAction> marketActionBidList = new ArrayList<MarketAction>();

					MarketAction myBid = new MarketAction();
					MarketAction myOffer = iterator.next();

					// chiamare un servizio che ritorni la MarketSession dato l'id
					MarketSession myMarketSession = Utilities.invokeGetMarketSession(
							myPayloadMEMO2MCM.getInformationBrokerServerUrl(), myPayloadMEMO2MCM.getTokenMCM(),
							myOffer.getMarketSessionid().intValue());

					myBid.setDate(myOffer.getDate());
					myBid.setActionStartTime(myOffer.getActionStartTime());
					myBid.setActionEndTime(myOffer.getActionEndTime());
					myBid.setValue(myOffer.getValue());
					myBid.setUom(myOffer.getUom());
					myBid.setPrice(myOffer.getPrice());
					myBid.setDeliveryPoint(myOffer.getDeliveryPoint());
					myBid.setMarketSessionid(myOffer.getMarketSessionid());
					MarketSession myMarketSession2 = Utilities.invokeGetMarketSession(
							myPayloadMEMO2MCM.getInformationBrokerServerUrl(), myPayloadMEMO2MCM.getTokenMCM(),
							myOffer.getMarketSessionid().intValue());

//					Marketplace myMarketplace = Utilities.invokeGetMarketplace(
//							myPayloadMEMO2MCM.getInformationBrokerServerUrl(), myPayloadMEMO2MCM.getTokenIB(),
//							myMarketSession2.getMarketplaceid().intValue());

					Marketplace myMarketplace = myMarketSession2.getMarketplaceid();

//					myBid.setMarketActorId(myMarketplace.getDSOId().intValue());
					myBid.setMarketActorid(myMarketplace.getDsoid().getId().intValue());

					myBid.setFormid(myOffer.getFormid());
					myBid.setActionTypeid(2);
					myBid.setStatusid(myOffer.getStatusid());

					marketActionBidList.add(myBid);

					URL url2 = new URL(myPayloadMEMO2MCM.getInformationBrokerServerUrl() + "/marketplace/"
							+ myMarketSession.getMarketplaceid().getId().intValue() + "/marketsessions/"
							+ myMarketSession.getId().intValue() + "/actions/");

					log.debug("Start - Rest url2: " + url2.toString());
					HttpURLConnection connService2 = (HttpURLConnection) url2.openConnection();
					connService2.setDoOutput(true);
					connService2.setRequestMethod("POST");
					connService2.setRequestProperty("Content-Type", "application/json");
					connService2.setRequestProperty("Accept", "application/json");
					connService2.setRequestProperty("Authorization", myPayloadMEMO2MCM.getTokenMCM());

					GsonBuilder gb2 = new GsonBuilder();
					gb2.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					Gson gson2 = gb2.create();
					String record2 = gson2.toJson(marketActionBidList);
					log.debug("Rest Request: " + record2);

					OutputStream os2 = connService2.getOutputStream();
					os2.write(record2.getBytes());

					int myResponseCode2 = connService2.getResponseCode();
					log.debug("HTTP error code :" + myResponseCode2);

					BufferedReader br2 = new BufferedReader(new InputStreamReader((connService2.getInputStream())));
					Type listType2 = new TypeToken<ArrayList<MarketAction>>() {
					}.getType();
					gb2 = new GsonBuilder();
					gb2.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					gson2 = gb2.create();
					ArrayList<MarketAction> returnMarketActionBidList = new ArrayList<MarketAction>();
					returnMarketActionBidList = gson2.fromJson(br2, listType2);
					log.debug("payload received: " + gson2.toJson(returnMarketActionBidList));

					os2.flush();
					os2.close();
					br2.close();
					connService2.disconnect();

					// Create list(1 item) with MarketActionCounterOffer with
					// current bid and offer and offer value
					// invoke POST "/marketplace/{marketId}/counteroffers" and
					// retrieve list(1 item) of updated MarketActionCounterOffer

					MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();
					myMarketActionCounterOffer
							.setMarketAction_Bid_id(returnMarketActionBidList.get(0).getId().intValue());
					myMarketActionCounterOffer.setMarketAction_Offer_id(myOffer.getId().intValue());

					myMarketActionCounterOffer.setExchangedValue(myOffer.getValue());

					ArrayList<MarketActionCounterOffer> marketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();
					ArrayList<MarketActionCounterOffer> tempMarketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();

					marketActionCounterOfferList.add(myMarketActionCounterOffer);

					URL url3 = new URL(myPayloadMEMO2MCM.getInformationBrokerServerUrl() + "/marketplace/"
							+ myMarketSession.getMarketplaceid().getId().intValue() + "/counteroffers/");

					log.debug("Start - Rest url3: " + url3.toString());
					HttpURLConnection connService3 = (HttpURLConnection) url3.openConnection();
					connService3.setDoOutput(true);
					connService3.setRequestMethod("POST");
					connService3.setRequestProperty("Content-Type", "application/json");
					connService3.setRequestProperty("Accept", "application/json");
					connService3.setRequestProperty("Authorization", myPayloadMEMO2MCM.getTokenMCM());

					GsonBuilder gb3 = new GsonBuilder();
					gb3.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					Gson gson3 = gb3.create();
					String record3 = gson3.toJson(marketActionCounterOfferList);
					log.debug("Rest Request: " + record3);

					OutputStream os3 = connService3.getOutputStream();
					os3.write(record3.getBytes());

					int myResponseCode3 = connService3.getResponseCode();
					log.debug("HTTP error code :" + myResponseCode3);

					BufferedReader br3 = new BufferedReader(new InputStreamReader((connService3.getInputStream())));
					Type listType3 = new TypeToken<ArrayList<MarketActionCounterOffer>>() {
					}.getType();

					tempMarketActionCounterOfferList = gson3.fromJson(br3, listType3);
					log.debug("payload received: " + gson3.toJson(returnMarketActionCounterOfferList));
					returnMarketActionCounterOfferList.add(tempMarketActionCounterOfferList.get(0));

				}

			}

		} catch (

		Exception e) {
			e.printStackTrace();
		}

		returnPayloadMEMO2MCM.setMarketActionCounterOfferList(returnMarketActionCounterOfferList);

		return returnPayloadMEMO2MCM;
	}

}
