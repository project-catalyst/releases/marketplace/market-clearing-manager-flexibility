package eu.catalyst.mcm.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mcm.payload.MarketSession;

public class Utilities {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Utilities.class);

//	public static Marketplace invokeGetMarketplace(String ibURL, String ibToken, int marketplaceid) throws IOException {
//
//		Marketplace result = null;
//		URL url = new URL(ibURL + "/marketplace/" + marketplaceid + "/");
//
//		System.out.println("ibURL - Rest url: " + url.toString());
//		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
//		connService.setDoOutput(true);
//		connService.setRequestMethod("GET");
//		connService.setRequestProperty("Accept", "application/json");
//		connService.setRequestProperty("Content-Type", "application/json");
//		connService.setRequestProperty("Authorization", ibToken);
//
//		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
//			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
//		}
//
//		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
//		Type listType = new TypeToken<Marketplace>() {
//		}.getType();
//		GsonBuilder gb = new GsonBuilder();
//		Gson gson = gb.create();
//		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
//		result = gson.fromJson(br, listType);
//		br.close();
//		connService.disconnect();
//		return result;
//	}

	public static MarketSession invokeGetMarketSession(String ibURL, String ibToken, int marketSessionId)
			throws IOException {

		MarketSession myMarketSession = null;
		URL url = new URL(ibURL + "/marketsessions/" + marketSessionId + "/");

		log.debug("invokeGetMarketSession - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

//		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
//		Type listType = new TypeToken<MarketSession>() {
//		}.getType();
//		GsonBuilder gb = new GsonBuilder();
//		Gson gson = gb.create();
//		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
//		result = gson.fromJson(br, listType);
//		br.close();

		InputStreamReader isReader = new InputStreamReader(connService.getInputStream());
		// Creating a BufferedReader object
		BufferedReader reader = new BufferedReader(isReader);
		StringBuffer sb = new StringBuffer();
		String str;
		while ((str = reader.readLine()) != null) {
			sb.append(str);
		}

		log.debug("payload received:: " + sb.toString());

		if (sb != null) {
			String strFrom = "dSOid";
			String strTo = "dsoid";
			int index = sb.indexOf(strFrom);
			if (index >= 0) {
				sb.replace(index, index + strFrom.length(), strTo);
				log.debug("payload modified: " + sb.toString());
				Type listType = new TypeToken<MarketSession>() {
				}.getType();
				GsonBuilder gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();
				myMarketSession = gson.fromJson(sb.toString(), listType);
			}

		}

		isReader.close();
		reader.close();

		connService.disconnect();
		return myMarketSession;
	}

}
