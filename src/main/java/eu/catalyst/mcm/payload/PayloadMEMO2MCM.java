package eu.catalyst.mcm.payload;

import java.util.List;

public class PayloadMEMO2MCM {

	public PayloadMEMO2MCM() {
	}

	private List<MarketAction> marketActionList;
	private List<MarketActionCounterOffer> marketActionCounterOfferList;

	private String informationBrokerServerUrl;
	private String tokenMEMO;
	private String marketClearingManagerUrl;
	private String tokenMCM;

	public String getInformationBrokerServerUrl() {
		return informationBrokerServerUrl;
	}

	public void setInformationBrokerServerUrl(String informationBrokerServerUrl) {
		this.informationBrokerServerUrl = informationBrokerServerUrl;
	}

	public String getTokenMEMO() {
		return tokenMEMO;
	}

	public void setTokenMEMO(String tokenMEMO) {
		this.tokenMEMO = tokenMEMO;
	}

	public String getTokenMCM() {
		return tokenMCM;
	}

	public void setTokenMCM(String tokenMCM) {
		this.tokenMCM = tokenMCM;
	}

	public String getMarketClearingManagerUrl() {
		return marketClearingManagerUrl;
	}

	public void setMarketClearingManagerUrl(String marketClearingManagerUrl) {
		this.marketClearingManagerUrl = marketClearingManagerUrl;
	}

	public List<MarketAction> getMarketActionList() {
		return marketActionList;
	}

	public void setMarketActionList(List<MarketAction> marketActionList) {
		this.marketActionList = marketActionList;
	}

	public List<MarketActionCounterOffer> getMarketActionCounterOfferList() {
		return marketActionCounterOfferList;
	}

	public void setMarketActionCounterOfferList(List<MarketActionCounterOffer> marketActionCounterOfferList) {
		this.marketActionCounterOfferList = marketActionCounterOfferList;
	}

}